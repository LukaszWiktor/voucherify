# Voucherify #

A RESTful voucher management API.

## Requirements ##

* Java 1.7+
* maven
* MongoDB

## Build & Run ##

* `mvn package`
* `java -jar target\voucherify-0.1-SNAPSHOT.jar server voucherify.yml`

By default the service is available at http://localhost:8080.
You can change it in the `voucherify.yml` file according to [dropwizard's specification](https://dropwizard.github.io/dropwizard/manual/configuration.html#connectors).

## The API ##

### Vouchers ###

**Sample Voucher**

```
#!json
{  
  "code":"kEGAVB3",
  "campaign":"vip",
  "discount":10.0,
  "discountType":"PERCENT",
  "expirationDate":"2015-12-31 23:59:59",
  "usage":{  
    "quantity":3,
    "usedQuantity":1
  },
  "additionalInfo":""
}
```

**Endpoints**

* `GET /vouchers` - list all vouchers
* `GET /vouchers/{code}` - get a voucher identified by given code
* `POST /vouchers` - create a voucher with a generated code
* `POST /vouchers/{code}` - create a voucher with a given code
* `DELETE /vouchers/{code}` - delete a voucher by given code
* `GET /vouchers/{code}/usage` - get voucher usage (initial and used quantity)
* `PUT /vouchers/{code}/usage` - increment usedQuantity

### Campaigns ###

**Sample Campaign**

```
#!json

{  
  "name":"vip",
  "expirationDate":"2015-12-31 23:59:59"
}
```
**Endpoints**

* `GET /campaigns` - list all campaigns
* `GET /campaigns/{name}` - get a campaign by name
* `POST /campaigns/{name}?vouchersCount=100&discount=10&discountType=AMOUNT&codePrefix=vip_` - create a campaign and generate vouchers
* `DELETE /campaigns/{name}` - delete a campaign with given name

### Examples of usage ###

** Create a voucher with given code **

Reuqest:

`POST /vouchers/valentines_2015`
```
#!json
{"discount" : 10.0, "discountType" : "AMOUNT", "expirationDate" : "2015-02-14 22:00:00"}
```
Response
```
#!json
{
    "code": "valentines_2015",
    "campaign": null,
    "discount": 10,
    "discountType": "AMOUNT",
    "expirationDate": "2015-02-14 22:00:00",
    "usage": {
        "quantity": 1,
        "usedQuantity": 0
    },
    "additionalInfo": null
}
```
or if a voucher with the specified code already exists:
```
#!json
{
    "code": 400,
    "message": "duplicate key",
    "details": null
}
```

** Create a voucher with generated code that can be used 3 times **

Reuqest:

`POST /vouchers`
```
#!json
{"discount" : 5.0, "discountType" : "PERCENT", "usage" : {"quantity" : 3}}
```
Response
```
#!json
{
    "code": "yKrhD",
    "campaign": null,
    "discount": 5,
    "discountType": "PERCENT",
    "expirationDate": null,
    "usage": {
        "quantity": 3,
        "usedQuantity": 0
    },
    "additionalInfo": null
}
```

** Use a voucher **

Request:

`PUT /vouchers/yKrhD/usage`

Response:
```
#!json
{
    "quantity": 3,
    "usedQuantity": 1
}
```
or if the voucher's usage limit has been exceeded:
```
#!json
{
    "code": 400,
    "message": "voucher quantity exceeded",
    "details": "yKrhD"
}
```

**Create a campaign and generate 100 vouchers with a code prefix**

Request:

`POST /vouchersCount=100&discount=25&codePrefix=vip_`
```
#!json
{"name" : "vip", "expirationDate" : "2015-04-01 00:00:00"}
```

Response: No Content