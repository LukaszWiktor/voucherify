package pl.luwi.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import pl.luwi.core.Campaign;
import pl.luwi.core.DiscountType;
import pl.luwi.core.Voucher;
import pl.luwi.exceptions.ResourceNotFoundException;
import pl.luwi.service.CampaignService;


@Path("/campaigns")
@Produces(MediaType.APPLICATION_JSON)
public class CampaignResource {

	private final CampaignService campaignService;
	
	public CampaignResource(CampaignService campaignService) {
		this.campaignService = campaignService;
	}
	
    @GET
    public List<Campaign> list() {
        return campaignService.list();
    }
    
    @GET
    @Path("/{name}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Campaign get(@PathParam("name") String name) throws ResourceNotFoundException {
    	return campaignService.getByName(name);
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(
    		@QueryParam("vouchersCount") int vouchersCount,
    		@QueryParam("codePrefix") String codePrefix,
    		@QueryParam("discount") double discount,
    		@QueryParam("discountType") @DefaultValue("PERCENT") DiscountType discountType,
    		Campaign campaign)
    				throws ResourceNotFoundException {
    	Voucher voucherTemplate = null;
    	if (vouchersCount > 0) {
    		voucherTemplate = new Voucher.Builder()
    			.setCode(codePrefix)
    			.setDiscount(discount)
    			.setDiscountType(discountType)
    			.build();
    	}
    	campaignService.create(campaign, vouchersCount, voucherTemplate);
    }
    
    @DELETE
    @Path("/{name}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("name") String name) {
    	if (campaignService.delete(name)) {
    		return Response.ok().build();
    	} else {
    		return Response.status(Status.NOT_FOUND).build();
    	}
    }
}
