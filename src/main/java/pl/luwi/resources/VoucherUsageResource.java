package pl.luwi.resources;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import pl.luwi.core.VoucherUsage;
import pl.luwi.exceptions.ResourceNotFoundException;
import pl.luwi.exceptions.VoucherQuantityExceededException;
import pl.luwi.service.VoucherService;

@Path("/vouchers/{code}/usage")
@Produces(MediaType.APPLICATION_JSON)
public class VoucherUsageResource {
	
	private final VoucherService voucherService;

	public VoucherUsageResource(VoucherService voucherService) {
		this.voucherService = voucherService;
	}
	
	@GET
	public VoucherUsage getUsage(@PathParam("code") String code) throws ResourceNotFoundException {
		return voucherService.getByCode(code).getUsage();
	}
	
	@PUT
	public VoucherUsage useVoucher(@PathParam("code") String code) throws ResourceNotFoundException, VoucherQuantityExceededException {
		voucherService.use(code);
		return getUsage(code);
	}
	
}
