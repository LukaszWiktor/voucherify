package pl.luwi.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import pl.luwi.core.Voucher;
import pl.luwi.exceptions.ResourceNotFoundException;
import pl.luwi.service.VoucherService;

@Path("/vouchers")
@Produces(MediaType.APPLICATION_JSON)
public class VouchersResource {

	private final VoucherService voucherService;

	public VouchersResource(VoucherService voucherService) {
		this.voucherService = voucherService;
	}
	
    @GET
    public List<Voucher> list() {
        return voucherService.list();
    }
    
    @GET
    @Path("/{code}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Voucher get(@PathParam("code") String code) throws ResourceNotFoundException {
    	return voucherService.getByCode(code);
    }
    
    /**
     * Create voucher with generated code.
     * 
     * @param voucher
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Voucher create(Voucher voucher) {
    	voucher = voucher.withCode(null);
    	return voucherService.create(voucher);
    }
    
    /**
     * Create voucher with given code.
     * 
     * @param code
     * @param voucher
     */
    @POST
    @Path("/{code}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Voucher create(@PathParam("code") String code, Voucher voucher) {
    	voucher = voucher.withCode(code);
    	return voucherService.create(voucher);
    }
    
    @DELETE
    @Path("/{code}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("code") String code) {
    	if (voucherService.delete(code)) {
    		return Response.ok().build();
    	} else {
    		return Response.status(Status.NOT_FOUND).build();
    	}
    }
    
}
