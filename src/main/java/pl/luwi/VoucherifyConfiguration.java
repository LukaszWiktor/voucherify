package pl.luwi;

import io.dropwizard.Configuration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class VoucherifyConfiguration extends Configuration {
    
	private final MongoDBConfig mongoConfig;
	
	@JsonCreator
	public VoucherifyConfiguration(
			@JsonProperty("mongo") MongoDBConfig mongoConfig) {
		this.mongoConfig = mongoConfig;
	}
	

	public MongoDBConfig getMongoConfig() {
		return mongoConfig;
	}

}
