package pl.luwi.core;

public enum DiscountType {

	AMOUNT, PERCENT;
}
