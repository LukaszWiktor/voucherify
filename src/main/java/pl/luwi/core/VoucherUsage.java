package pl.luwi.core;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

public class VoucherUsage {

	private final int quantity;
	private final int usedQuantity;
	
	@JsonCreator
	public VoucherUsage(
			@JsonProperty("quantity") Integer quantity,
			@JsonProperty("usedQuantity") Integer usedQuantity) {
		this.quantity = Optional.fromNullable(quantity).or(1);
		this.usedQuantity = Optional.fromNullable(usedQuantity).or(0);
		Preconditions.checkArgument(this.usedQuantity <= this.quantity, "exceeded quantity");
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public int getUsedQuantity() {
		return usedQuantity;
	}
	
}
