package pl.luwi.core;

import java.util.Date;

import org.mongojack.ObjectId;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Campaign {

	private final String id;
	
	private final String name;
	
	private Date expirationDate;

	@JsonCreator
	public Campaign(
			@ObjectId @JsonProperty("_id") String id,
			@JsonProperty("name") String name,
			@JsonProperty("expirationDate") Date expirationDate) {
		this.id = id;
		this.name = name;
		this.expirationDate = expirationDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getName() {
		return name;
	}
	
	
	
}
