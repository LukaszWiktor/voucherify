package pl.luwi.core;

import java.util.Date;
import java.util.Objects;

import org.mongojack.ObjectId;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Optional;

public class Voucher {
	
	private final String id;
	
	private final String code;
	
	private final String campaign;
	
	private final double discount;
	
	private final DiscountType discountType;
	
	private final Date expirationDate;
	
	private final VoucherUsage usage;
	
	private final String additionalInfo;

	@JsonCreator
	public Voucher(
			@ObjectId @JsonProperty("_id") String id,
			@JsonProperty("code") String code, 
			@JsonProperty("campaign") String campaign,
			@JsonProperty("discount") double discount,
			@JsonProperty("discountType") DiscountType discountType, 
			@JsonProperty("expirationDate") Date expirationDate,
			@JsonProperty("usage") VoucherUsage usage,
			@JsonProperty("additionalInfo") String additionalInfo) {
		this.id = id;
		this.code = code;
		this.campaign = campaign;
		this.discount = discount;
		this.discountType = Objects.requireNonNull(discountType, "discountType is required");
		this.expirationDate = expirationDate;
		this.usage = Optional.fromNullable(usage).or(new VoucherUsage(1, 0));
		this.additionalInfo = additionalInfo;
	}

	public String getCode() {
		return code;
	}
	
	public Voucher withCode(String newCode) {
		return new Voucher(id, newCode, campaign, discount, discountType, expirationDate, usage, additionalInfo);
	}

	public String getCampaign() {
		return campaign;
	}
	
	public Voucher withCampaign(String newCampaign) {
		return new Voucher(id, code, newCampaign, discount, discountType, expirationDate, usage, additionalInfo);
	}

	public double getDiscount() {
		return discount;
	}

	public DiscountType getDiscountType() {
		return discountType;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}
	
	public Voucher withExpirationDate(Date newExpirationDate) {
		return new Voucher(id, code, campaign, discount, discountType, newExpirationDate, usage, additionalInfo);
	}
	
	public VoucherUsage getUsage() {
		return usage;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public static class Builder {
		
		private String id;
		
		private String code;
		
		private String campaign;
		
		private double discount;
		
		private DiscountType discountType;
		
		private Date expirationDate;
		
		private VoucherUsage usage;
		
		private String additionalInfo;

		public Builder setId(String id) {
			this.id = id;
			return this;
		}
		
		public Builder setCode(String code) {
			this.code = code;
			return this;
		}

		public Builder setCampaign(String campaign) {
			this.campaign = campaign;
			return this;
		}

		public Builder setDiscount(double discount) {
			this.discount = discount;
			return this;
		}

		public Builder setDiscountType(DiscountType discountType) {
			this.discountType = discountType;
			return this;
		}

		public Builder setExpirationDate(Date expirationDate) {
			this.expirationDate = expirationDate;
			return this;
		}
		
		public Builder setUsage(VoucherUsage usage) {
			this.usage = usage;
			return this;
		}

		public Builder setAdditionalInfo(String additionalInfo) {
			this.additionalInfo = additionalInfo;
			return this;
		}
		
		public Voucher build() {
			return new Voucher(id, code, campaign, discount, discountType, expirationDate, usage, additionalInfo);
		}
				
	}

}
