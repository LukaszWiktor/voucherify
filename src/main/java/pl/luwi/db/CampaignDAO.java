package pl.luwi.db;

import java.util.List;

import pl.luwi.core.Campaign;

import com.google.common.base.Optional;

public interface CampaignDAO {
	
	List<Campaign> list();
	
	Optional<Campaign> getByName(String name);

	void create(Campaign campaign);
	
	boolean delete(String name);
}
