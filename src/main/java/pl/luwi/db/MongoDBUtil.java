package pl.luwi.db;

import io.dropwizard.jackson.Jackson;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.mongojack.JacksonDBCollection;
import org.mongojack.internal.MongoJackModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.luwi.MongoDBConfig;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

public class MongoDBUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(MongoDBConfig.class);

    private static final ObjectMapper OBJECT_MAPPER = configureObjectMapper();

    private static ObjectMapper configureObjectMapper() {
        ObjectMapper om = Jackson.newObjectMapper(); // same config as dropwizard
        MongoJackModule.configure(om); // plus mongojack-specific settings
        return om;
    }
    
    public static MongoClient createMongoClient(MongoDBConfig mongoConfig) throws UnknownHostException {
        ServerAddress serverAddress = new ServerAddress(mongoConfig.getHost(), mongoConfig.getPort());
        LOGGER.info("Obtaining mongo server at address {}:{}", serverAddress.getHost(), serverAddress.getPort());
        MongoClient mongoClient = null;
        if (mongoConfig.getUser() != null && mongoConfig.getPassword() != null) {
            LOGGER.info("Obtaining mongo db with AUTH");
            MongoCredential cred = MongoCredential.createPlainCredential(
            		mongoConfig.getUser() , mongoConfig.getDbName(), mongoConfig.getPassword().toCharArray());
            List<MongoCredential> auths = new ArrayList<MongoCredential>();
            auths.add(cred);
            mongoClient = new MongoClient(serverAddress, auths);
        } else {
            LOGGER.info("Obtaining mongo db");
            mongoClient = new MongoClient(serverAddress);
        }
        return mongoClient;
    }
    
    static <T, ID> JacksonDBCollection<T, ID> createCollection(DB mongoDB, String name, Class<T> type, Class<ID> idType) {
        return JacksonDBCollection.wrap(mongoDB.getCollection(name), type, idType, OBJECT_MAPPER);
    }

}
