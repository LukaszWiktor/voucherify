package pl.luwi.db;

import java.util.List;

import org.mongojack.DBQuery;
import org.mongojack.DBUpdate;
import org.mongojack.JacksonDBCollection;
import org.mongojack.WriteResult;

import pl.luwi.core.Voucher;

import com.google.common.base.Optional;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

public class MongoVoucherDAO implements VoucherDAO {

	private final JacksonDBCollection<Voucher, String> vouchersCollection;
	
	public MongoVoucherDAO(DB mongoDB) {
		vouchersCollection = MongoDBUtil.createCollection(mongoDB, "vouchers", Voucher.class, String.class);
		vouchersCollection.createIndex(new BasicDBObject("code", 1), new BasicDBObject("unique", true));
		vouchersCollection.createIndex(new BasicDBObject("expirationDate", 1), new BasicDBObject("expireAfterSeconds", 0));
	}

	@Override
	public List<Voucher> list() {
		return vouchersCollection.find().toArray();
	}
	
	private DBObject byCode(String code) {
		return new BasicDBObject("code", code);
	}
	
	@Override
	public Optional<Voucher> getByCode(String code) {
		return Optional.fromNullable(vouchersCollection.findOne(byCode(code)));
	}
	
	@Override
	public boolean exists(String code) {
		return vouchersCollection.count(byCode(code)) > 0;
	}

	@Override
	public void create(Voucher voucher) {
		vouchersCollection.insert(voucher);
	}
	
	@Override
	public boolean delete(String code) {
		WriteResult<Voucher, String> result = vouchersCollection.remove(byCode(code));
		return result.getN() > 0;
	}
	
	@Override
	public void deleteByCampaign(String campaignName) {
		vouchersCollection.remove(DBQuery.is("campaign", campaignName));
	}
	
	@Override
	public boolean use(String code) {
		WriteResult<Voucher, String> result = vouchersCollection.update(
				DBQuery.is("code", code).and(
				DBQuery.where("this.usage.quantity > this.usage.usedQuantity")),
				DBUpdate.inc("usage.usedQuantity"));
		return result.getN() > 0;
	}
}
