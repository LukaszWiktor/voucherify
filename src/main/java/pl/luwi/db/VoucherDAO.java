package pl.luwi.db;

import java.util.List;

import pl.luwi.core.Voucher;

import com.google.common.base.Optional;


public interface VoucherDAO {
	
	List<Voucher> list();
	
	Optional<Voucher> getByCode(String code);
	
	boolean exists(String code);
	
	void create(Voucher voucher);
	
	boolean delete(String code);

	void deleteByCampaign(String campaignName);

	boolean use(String code);
	
}
