package pl.luwi.db;

import java.util.List;

import org.mongojack.JacksonDBCollection;
import org.mongojack.WriteResult;

import pl.luwi.core.Campaign;

import com.google.common.base.Optional;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

public class MongoCampaignDAO implements CampaignDAO {

	private final JacksonDBCollection<Campaign, String> campaignsCollection;
	
	public MongoCampaignDAO(DB mongoDB) {
		campaignsCollection = MongoDBUtil.createCollection(mongoDB, "campaigns", Campaign.class, String.class);
		campaignsCollection.createIndex(new BasicDBObject("name", 1), new BasicDBObject("unique", true));
		campaignsCollection.createIndex(new BasicDBObject("expirationDate", 1), new BasicDBObject("expireAfterSeconds", 0));
	}

	@Override
	public List<Campaign> list() {
		return campaignsCollection.find().toArray();
	}
	
	private DBObject byName(String name) {
		return new BasicDBObject("name", name);
	}
	
	@Override
	public Optional<Campaign> getByName(String name) {
		return Optional.fromNullable(campaignsCollection.findOne(byName(name)));
	}
	

	@Override
	public void create(Campaign campaign) {
		campaignsCollection.insert(campaign);
	}
	
	@Override
	public boolean delete(String code) {
		WriteResult<Campaign, String> result = campaignsCollection.remove(byName(code));
		return result.getN() > 0;
	}

}
