package pl.luwi;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Optional;

public class MongoDBConfig {
	
	private final String host;
	private final int port;
	private final String dbName;
	private final String user;
	private final String password;
	
	@JsonCreator
	public MongoDBConfig(
			@JsonProperty("host") String host,
			@JsonProperty("port") Integer port, 
			@JsonProperty("dbName") String dbName,
			@JsonProperty("user") String user,
			@JsonProperty("password") String password) {
		this.host = Optional.fromNullable(host).or("localhost");
		this.port = Optional.fromNullable(port).or(27017);
		this.dbName = Optional.fromNullable(dbName).or("voucherify");
		this.user = user;
		this.password = password;
	}

	public String getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	public String getDbName() {
		return dbName;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public String toString() {
		return String
				.format("[host=%s, port=%s, dbName=%s, user=%s, password=%s]",
						host, port, dbName, user, password);
	}

}
