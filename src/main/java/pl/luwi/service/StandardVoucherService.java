package pl.luwi.service;

import java.util.List;

import pl.luwi.core.Voucher;
import pl.luwi.db.VoucherDAO;
import pl.luwi.exceptions.ResourceNotFoundException;
import pl.luwi.exceptions.VoucherQuantityExceededException;

import com.google.common.base.Optional;

public class StandardVoucherService implements VoucherService {

	private final VoucherDAO voucherDAO;
	private final CodeGenerator codeGenerator;
	
	public StandardVoucherService(VoucherDAO voucherDAO, CodeGenerator codeGenerator) {
		this.voucherDAO = voucherDAO;
		this.codeGenerator = codeGenerator;
	}

	public List<Voucher> list() {
		return voucherDAO.list();
	}

	public Voucher getByCode(String code) throws ResourceNotFoundException {
		Optional<Voucher> voucher = voucherDAO.getByCode(code);
		if (voucher.isPresent()) {
			return voucher.get();
		} else {
			throw new ResourceNotFoundException(code);
		}
	}

	public Voucher create(Voucher voucher) {
		return create(voucher, null);
	}
	
	public Voucher create(Voucher voucher, String codePrefix) {
		if (voucher.getCode() == null) {
			String code = generateUniqueCode();
			if (codePrefix != null) {
				code = codePrefix + code;
			}
			voucher = voucher.withCode(code);
		}
		voucherDAO.create(voucher);
		return voucher;
	}

	private String generateUniqueCode() {
		String code = null;
		int maxAttempts = 100;
		int attemptsCount = 0;
		do {
			code = codeGenerator.generate();
			if (attemptsCount++ > maxAttempts) {
				throw new RuntimeException("Failed to genereate unique code.");
			};
		} while (voucherDAO.exists(code));
		return code;
	}

	public boolean delete(String code) {
		return voucherDAO.delete(code);
	}
	
	@Override
	public void deleteByCampaign(String campaignName) {
		voucherDAO.deleteByCampaign(campaignName);
	}
	
	@Override
	public void use(String code) throws ResourceNotFoundException, VoucherQuantityExceededException {
		if (!voucherDAO.use(code)) {
			if (voucherDAO.exists(code)) {
				throw new VoucherQuantityExceededException(code);
			} else {
				throw new ResourceNotFoundException(code);
			}
		}
	}
}
