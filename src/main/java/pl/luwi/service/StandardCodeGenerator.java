package pl.luwi.service;

import java.util.Random;

import com.google.common.base.Preconditions;


public class StandardCodeGenerator implements CodeGenerator {

	private static final char[] CHARS="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
	private static final Random RND = new Random(System.currentTimeMillis());
	
	private final int minLen;
	private final int span;
	
	public StandardCodeGenerator(int minLen, int maxLen) {
		Preconditions.checkArgument(minLen > 0);
		Preconditions.checkArgument(maxLen > 0);
		Preconditions.checkArgument(maxLen >= minLen);
		this.minLen = minLen;
		this.span = maxLen - minLen;
	}

	@Override
	public String generate() {
		int length = minLen + RND.nextInt(span);
		return generate(length);
	}
	
	String generate(int length) {
		Preconditions.checkArgument(length > 0);
		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++) {
			sb.append(CHARS[RND.nextInt(CHARS.length)]);
		}
		return sb.toString();
	}


}
