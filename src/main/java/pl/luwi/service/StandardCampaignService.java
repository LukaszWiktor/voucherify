package pl.luwi.service;

import java.util.List;

import pl.luwi.core.Campaign;
import pl.luwi.core.Voucher;
import pl.luwi.db.CampaignDAO;
import pl.luwi.exceptions.ResourceNotFoundException;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

public class StandardCampaignService implements CampaignService {

	private final CampaignDAO campaignDAO;
	private final VoucherService voucherService;

	public StandardCampaignService(CampaignDAO campaignDAO,	VoucherService voucherService) {
		this.campaignDAO = campaignDAO;
		this.voucherService = voucherService;
	}

	public List<Campaign> list() {
		return campaignDAO.list();
	}

	public Campaign getByName(String name) throws ResourceNotFoundException {
		Optional<Campaign> campaign = campaignDAO.getByName(name);
		if (campaign.isPresent()) {
			return campaign.get();
		} else {
			throw new ResourceNotFoundException(name);
		}
	}

	public void create(Campaign campaign, int vouchersCount, Voucher voucherTemplate) {
		campaignDAO.create(campaign);
		
		if (vouchersCount > 0) {
			Preconditions.checkNotNull(voucherTemplate, "voucher template");
			String codePrefix = voucherTemplate.getCode();
			voucherTemplate = voucherTemplate
					.withCode(null)
					.withCampaign(campaign.getName())
					.withExpirationDate(campaign.getExpirationDate());
			for (int i = 0; i < vouchersCount; i++) {
				voucherService.create(voucherTemplate, codePrefix);
			}
		}
	}

	public boolean delete(String name) {
		boolean removed = campaignDAO.delete(name);
		if (removed) {
			voucherService.deleteByCampaign(name);
		}
		return removed;
	}
}
