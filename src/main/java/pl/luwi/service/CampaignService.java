package pl.luwi.service;

import java.util.List;

import pl.luwi.core.Campaign;
import pl.luwi.core.Voucher;
import pl.luwi.exceptions.ResourceNotFoundException;

public interface CampaignService {

	List<Campaign> list();

	Campaign getByName(String name) throws ResourceNotFoundException;

	void create(Campaign campaign, int vouchersCount, Voucher voucherTemplate);

	boolean delete(String name);

}