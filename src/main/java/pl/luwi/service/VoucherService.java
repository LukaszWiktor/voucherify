package pl.luwi.service;

import java.util.List;

import pl.luwi.core.Voucher;
import pl.luwi.exceptions.ResourceNotFoundException;
import pl.luwi.exceptions.VoucherQuantityExceededException;

public interface VoucherService {

	List<Voucher> list();

	Voucher getByCode(String code) throws ResourceNotFoundException;

	Voucher create(Voucher voucher);
	
	Voucher create(Voucher voucher, String codePrefix);

	boolean delete(String code);
	
	void deleteByCampaign(String name);

	void use(String code) throws ResourceNotFoundException, VoucherQuantityExceededException;

}