package pl.luwi.service;

public interface CodeGenerator {

	public String generate();
}