package pl.luwi;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import java.net.UnknownHostException;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.luwi.db.MongoCampaignDAO;
import pl.luwi.db.MongoDBUtil;
import pl.luwi.db.MongoVoucherDAO;
import pl.luwi.exceptions.MongoDuplicateKeyExceptionMapper;
import pl.luwi.exceptions.ResourceNotFoundExceptionMapper;
import pl.luwi.exceptions.VoucherQuantityExceededExceptionMapper;
import pl.luwi.health.MongoHealthCheck;
import pl.luwi.resources.CampaignResource;
import pl.luwi.resources.VoucherUsageResource;
import pl.luwi.resources.VouchersResource;
import pl.luwi.service.CampaignService;
import pl.luwi.service.StandardCampaignService;
import pl.luwi.service.StandardCodeGenerator;
import pl.luwi.service.StandardVoucherService;
import pl.luwi.service.VoucherService;

import com.mongodb.DB;
import com.mongodb.MongoClient;

public class VoucherifyApplication extends Application<VoucherifyConfiguration> {

    private static final Logger LOGGER = LoggerFactory.getLogger(VoucherifyApplication.class);
	
    public static void main(final String[] args) throws Exception {
        new VoucherifyApplication().run(args);
    }

    @Override
    public String getName() {
        return "Voucherify";
    }

    @Override
    public void initialize(final Bootstrap<VoucherifyConfiguration> bootstrap) {
        bootstrap.getObjectMapper().setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
    }

    @Override
    public void run(final VoucherifyConfiguration configuration, final Environment environment) {
    	LOGGER.info("MongoDB configurtion {}", configuration.getMongoConfig());
    	MongoClient mongoClient = null;
    	DB mongoDB = null;
    	try {
			mongoClient = MongoDBUtil.createMongoClient(configuration.getMongoConfig());
			mongoDB = mongoClient.getDB(configuration.getMongoConfig().getDbName());
		} catch (UnknownHostException e) {
			throw new RuntimeException("Cannot connect to MongoDB", e);
		}
    	
    	// --- lifecycle ---
		environment.lifecycle().manage(new ManagedMongoClient(mongoClient));
    	
    	// --- services --
    	
		VoucherService voucherService = createVoucherService(mongoDB);
    	CampaignService campaignService = createCampaignService(mongoDB, voucherService);
    	
    	// --- resources ---
    	environment.jersey().register(new VouchersResource(voucherService));
    	environment.jersey().register(new VoucherUsageResource(voucherService));
    	environment.jersey().register(new CampaignResource(campaignService));
    	
    	// --- exception mappers ----
    	environment.jersey().register(MongoDuplicateKeyExceptionMapper.class);
    	environment.jersey().register(ResourceNotFoundExceptionMapper.class);
    	environment.jersey().register(VoucherQuantityExceededExceptionMapper.class);
    	
    	// --- health checks ---
    	environment.healthChecks().register("mongo", new MongoHealthCheck(mongoClient));
    }

	private VoucherService createVoucherService(DB mongoDB) {
    	return new StandardVoucherService(new MongoVoucherDAO(mongoDB), new StandardCodeGenerator(5, 8));
    }
    
    
    private CampaignService createCampaignService(DB mongoDB, VoucherService voucherService) {
		return new StandardCampaignService(new MongoCampaignDAO(mongoDB), voucherService);
	}
}
