package pl.luwi.exceptions;

import io.dropwizard.jersey.errors.ErrorMessage;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class VoucherQuantityExceededExceptionMapper implements ExceptionMapper<VoucherQuantityExceededException> {

	@Override
	public Response toResponse(VoucherQuantityExceededException exception) {
        final ErrorMessage errorMessage = new ErrorMessage(Response.Status.BAD_REQUEST.getStatusCode(),
                "voucher quantity exceeded", exception.getCode());
        
        return Response.status(Response.Status.BAD_REQUEST)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .entity(errorMessage)
                .build();
	}

}
