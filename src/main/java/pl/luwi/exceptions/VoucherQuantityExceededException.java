package pl.luwi.exceptions;

public class VoucherQuantityExceededException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private final String code;

	public VoucherQuantityExceededException(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
}
