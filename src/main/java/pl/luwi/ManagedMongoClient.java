package pl.luwi;

import io.dropwizard.lifecycle.Managed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.MongoClient;

public class ManagedMongoClient implements Managed {

	private static final Logger LOGGER = LoggerFactory.getLogger(ManagedMongoClient.class);
	
    private final MongoClient mongoClient;

    public ManagedMongoClient(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }

    public void start() throws Exception {
    }

    public void stop() throws Exception {
    	LOGGER.info("Shutting down mongo client.");
        mongoClient.close();
    }
}