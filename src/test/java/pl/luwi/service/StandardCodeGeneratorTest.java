package pl.luwi.service;

import org.testng.Assert;
import org.testng.annotations.Test;

public class StandardCodeGeneratorTest {

	@Test
	public void shouldGenerateRandomCodeAtGivenLength() {
		StandardCodeGenerator codeGenerator = new StandardCodeGenerator(1,10);
		for (int i = 1; i < 10; i++) {
			String code = codeGenerator.generate(i);
			Assert.assertEquals(code.length(), i);
		}
	}
}
