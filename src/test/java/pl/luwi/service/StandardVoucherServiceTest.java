package pl.luwi.service;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.Test;

import pl.luwi.core.DiscountType;
import pl.luwi.core.Voucher;
import pl.luwi.db.VoucherDAO;

public class StandardVoucherServiceTest {

	@Test
	public void shouldCreateVoucherWithGeneratedCode() {
		VoucherDAO daoMock = Mockito.mock(VoucherDAO.class);
		CodeGenerator codeGenMock = Mockito.mock(CodeGenerator.class);
		Mockito.when(codeGenMock.generate()).thenReturn("dubidu");
		
		StandardVoucherService service = new StandardVoucherService(daoMock, codeGenMock);
		
		Voucher voucher = new Voucher.Builder()
			.setDiscount(10.0)
			.setDiscountType(DiscountType.AMOUNT)
			.build();
		
		Voucher createdVoucher = service.create(voucher);
		Assert.assertEquals(createdVoucher.getCode(), "dubidu");
		Assert.assertEquals(createdVoucher.getDiscount(), voucher.getDiscount());
		Assert.assertEquals(createdVoucher.getDiscountType(), voucher.getDiscountType());
	}
	
	@Test
	public void shouldCreateVoucherWithUniqueCode() {
		VoucherDAO daoMock = Mockito.mock(VoucherDAO.class);
		Mockito.when(daoMock.exists("a")).thenReturn(true);
		Mockito.when(daoMock.exists("b")).thenReturn(true);
		Mockito.when(daoMock.exists("c")).thenReturn(false);
		
		CodeGenerator codeGenMock = Mockito.mock(CodeGenerator.class);
		Mockito.when(codeGenMock.generate()).thenReturn("a", "b", "c");
		
		StandardVoucherService service = new StandardVoucherService(daoMock, codeGenMock);
		
		Voucher voucher = new Voucher.Builder()
			.setDiscount(10.0)
			.setDiscountType(DiscountType.AMOUNT)
			.build();
		
		Voucher createdVoucher = service.create(voucher);
		Assert.assertEquals(createdVoucher.getCode(), "c");
		Assert.assertEquals(createdVoucher.getDiscount(), voucher.getDiscount());
		Assert.assertEquals(createdVoucher.getDiscountType(), voucher.getDiscountType());	
	}
	
	@Test(expectedExceptions=RuntimeException.class, 
	      expectedExceptionsMessageRegExp="Failed to genereate unique code.",
	      timeOut=1000)
	public void shouldPreventInfiniteLoopWhenNoUniqueCode() {
		VoucherDAO daoMock = Mockito.mock(VoucherDAO.class);
		Mockito.when(daoMock.exists("all_codes_taken")).thenReturn(true);
		
		CodeGenerator codeGenMock = Mockito.mock(CodeGenerator.class);
		Mockito.when(codeGenMock.generate()).thenReturn("all_codes_taken");
		
		new StandardVoucherService(daoMock, codeGenMock)
			.create(new Voucher.Builder()
				.setDiscount(10.0)
				.setDiscountType(DiscountType.AMOUNT)
				.build());
	}
}
